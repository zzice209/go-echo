package controller

import (
	"github.com/labstack/echo/v4"
	"github.com/zzice209/go-echo/model"
	"github.com/zzice209/go-echo/storage"
	"net/http"
)

// Get Students

func GetStudents(c echo.Context) error {
	students, _ := GetRepoStudents()
	return c.JSON(http.StatusOK, students)
}

func GetRepoStudents() ([]model.Student, error) {
	db := storage.GetDBInstance()
	students := []model.Student{}

	if err := db.Find(&students).Error; err != nil {
		return nil, err
	}

	return students, nil
}
