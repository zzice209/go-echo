module github.com/zzice209/go-echo

go 1.15

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/labstack/echo/v4 v4.1.17
	gorm.io/driver/postgres v1.0.5 // indirect
	gorm.io/gorm v1.20.7 // indirect
)
