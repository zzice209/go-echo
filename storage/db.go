package storage

import (
	"github.com/jinzhu/gorm"
	"github.com/zzice209/go-echo/config"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)

var DB *gorm.DB

func NewDb(params ...string) *gorm.DB {
	var err error
	conString := config.GetPostgresConnectionString()

	log.Println(conString)

	DB, err = gorm.Open(config.GetDBType(), conString)

	if err != nil {
		log.Panic(err)
	}

	return DB
}

func GetDBInstance() *gorm.DB {
	return DB
}
